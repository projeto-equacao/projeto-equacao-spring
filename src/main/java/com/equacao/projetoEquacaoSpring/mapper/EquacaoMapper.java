package com.equacao.projetoEquacaoSpring.mapper;

import com.equacao.projetoEquacaoSpring.controller.dto.request.CreateEquacaoRequest;
import com.equacao.projetoEquacaoSpring.controller.dto.request.ListaEquacaoRequest;
import com.equacao.projetoEquacaoSpring.controller.dto.response.ListaEquacaoResponse;
import com.equacao.projetoEquacaoSpring.entity.EquacaoEntity;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EquacaoMapper {

    private EquacaoMapper() {
    }

    public static EquacaoMapper newInstance() {
        return new EquacaoMapper();
    }

    public ListaEquacaoResponse convertEntity(EquacaoEntity entity) {
        log.info("Converting Entity: {} for Entity", entity);
        return new ListaEquacaoResponse(entity.getDelta(), entity.getX1(), entity.getX2(), entity.getMessage());
    }

    public EquacaoEntity createEntity(CreateEquacaoRequest request) {
        log.info("Converting Request: {} for Entity", request);
        return new EquacaoEntity(request.a(), request.b(), request.c(),
                request.delta(), request.x1(), request.x2(), request.message());
    }

    public EquacaoEntity createEquacao(ListaEquacaoRequest request, ListaEquacaoResponse response) {
        log.info("Converting Request: {} and response {} for Entity", request, response);
        return new EquacaoEntity(request.a(), request.b(), request.c(),
                response.delta(), response.x1(), response.x2(), response.message());
    }
}
