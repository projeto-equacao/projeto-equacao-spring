package com.equacao.projetoEquacaoSpring.repository;

import com.equacao.projetoEquacaoSpring.entity.EquacaoEntity;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EquacaoRepository extends ListCrudRepository<EquacaoEntity, String> {

}
