package com.equacao.projetoEquacaoSpring.controller;

import com.equacao.projetoEquacaoSpring.controller.dto.request.CreateEquacaoRequest;
import com.equacao.projetoEquacaoSpring.controller.dto.request.ListaEquacaoRequest;
import com.equacao.projetoEquacaoSpring.controller.dto.response.CreateEquacaoResponse;
import com.equacao.projetoEquacaoSpring.controller.dto.response.ListaEquacaoResponse;
import com.equacao.projetoEquacaoSpring.entity.EquacaoEntity;
import com.equacao.projetoEquacaoSpring.service.EquacaoService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("equacao")
@RequiredArgsConstructor
public class EquacaoController {

    @Autowired
    private final EquacaoService service;

    /// teste
    @GetMapping
    public String idteste() {
        return "Olá mundo! Estou novamente no aguardo, ok";
    }

    //////////////////////////////////////////// Um novo teste
    @GetMapping("outroTeste")
    public ResponseEntity getAllEquacao() {
        return ResponseEntity.ok("tudo cento");
    }

    //////////////////////// ^^^^^^^^^^^^^^^^^^^^^^^^^^^testes que funcionam no isomnia.
    @GetMapping("/listar")
    public List<ListaEquacaoResponse> listar() {
        return service.listar();
    }

    /////
    @PostMapping // gravando algo
    public CreateEquacaoResponse createEquacao(@RequestBody @Validated CreateEquacaoRequest request) {
        service.createEquacao(request);
        return null;
    }

    @PostMapping("/calcular")
    public ListaEquacaoResponse calcular(@RequestBody @Validated ListaEquacaoRequest request) {
        return service.calcular(request);
    }

    @DeleteMapping("/delete/{id}")
    public List<EquacaoEntity> deleteById(@PathVariable @RequestBody @Validated  String id) {
        service.deleteById(id);
        return null;
    }
}



