package com.equacao.projetoEquacaoSpring.controller.dto.request;

public record ListaEquacaoRequest(double a, double b, double c) {
}
