package com.equacao.projetoEquacaoSpring.controller.dto.response;

public record ListaEquacaoResponse(Double delta, Double x1, Double x2, String message) {
}
