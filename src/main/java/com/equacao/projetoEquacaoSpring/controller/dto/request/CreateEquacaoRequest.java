package com.equacao.projetoEquacaoSpring.controller.dto.request;

public record CreateEquacaoRequest(double a, double b, double c, Double delta, Double x1, Double x2, String message) { }
