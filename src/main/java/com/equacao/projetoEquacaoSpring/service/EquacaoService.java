package com.equacao.projetoEquacaoSpring.service;

import com.equacao.projetoEquacaoSpring.controller.dto.request.CreateEquacaoRequest;
import com.equacao.projetoEquacaoSpring.controller.dto.request.ListaEquacaoRequest;
import com.equacao.projetoEquacaoSpring.controller.dto.response.ListaEquacaoResponse;
import com.equacao.projetoEquacaoSpring.entity.EquacaoEntity;
import com.equacao.projetoEquacaoSpring.mapper.EquacaoMapper;
import com.equacao.projetoEquacaoSpring.repository.EquacaoRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author waldir
 * Classe serviços - Requerimento e Resposta
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class EquacaoService {

    @Autowired
    private final EquacaoRepository repository;
    private final int PLACES = 2; // esta variável é para que tenha dois casas decimais.

    public List<ListaEquacaoResponse> listar() {
        log.info("Looking for equations");
        List<ListaEquacaoResponse> response = repository.findAll()
                .stream()
                .map(entity -> EquacaoMapper.newInstance().convertEntity(entity))
                .toList();
        log.info("Equations found: {}", response);
        return response;
    }

    public void createEquacao(CreateEquacaoRequest request) {
        log.info("Creating an equation. Request: {}", request);
        EquacaoEntity entity = EquacaoMapper.newInstance().createEntity(request);

        log.info("Saving entity in the base. Entity: {}", entity);
        entity.setId(new ObjectId().toString());
        repository.save(entity);
    }

    public void deleteById(String id) {
        log.info("Find the equation to delete. Entity: {}", id);
        List<ListaEquacaoResponse> numeroId = repository.findAll()
                .stream()
                .map(entity -> EquacaoMapper.newInstance().convertEntity(entity))
                .toList();
        log.info("Equation deleted: {}", numeroId);
        repository.deleteById(id);
    }

    public ListaEquacaoResponse calcular(@RequestBody ListaEquacaoRequest request) {
        String message = null;
        if (request.a() == 0) {
            log.warn("Error in typing the coefficient 'A'.");
            return new ListaEquacaoResponse(null, null, null, "Equação invalida! Coeficiente \'A\' não pode ser zero (0)");
        }

        double delta = calcularDelta(request);
        if (delta < 0) {
            message = """
                    Quando Delta menor que zero (0)! 
                    Não existem raízes.
                    """;
            return new ListaEquacaoResponse(delta, null, null, message);
        }

        double x1 = calcularX1(request, delta);
        double x2 = calcularX2(request, delta);
        if (delta == 0) {
            message = """
                    Quando Delta igual a zero (0)!
                    O conjunto solução terá valores iguais para X1 e X2.
                    Valores encontrados com SUCESSO.
                    """;

        } else if ((x1 != 0) || (x2 != 0)) {
            message = "Encontrados com SUCESSO.";
        }

        if ((request.b() == 0) || (request.c() == 0)) {
            message = "Equação do 2º Grau INCOMPLETA. Porém, VALIDA!";
        }

        ListaEquacaoResponse response = new ListaEquacaoResponse(delta, x1, x2, message);
        EquacaoEntity entity = EquacaoMapper.newInstance().createEquacao(request, response);
        repository.save(entity);
        return response;
    }

    public double calcularDelta(ListaEquacaoRequest request) {
        log.info("Calculating delta. Request: {}", request);
        Double delta = (Math.pow(request.b(), 2) - 4 * request.a() * request.c());
        log.info("The response value of delta is: {}", delta);
        return (delta);
    }

    public double calcularX1(ListaEquacaoRequest request, double delta) {
        double vlX1 = ((-request.b() + Math.sqrt(delta)) / (2 * request.a()));
        log.info("The value of X1 is: {}", vlX1);
        return vlX1;
    }

    public double calcularX2(ListaEquacaoRequest request, double delta) {
        double vlX2 = (-request.b() - Math.sqrt(delta)) / (2 * request.a());
        log.info("The value of X2 is: {}", vlX2);
        return vlX2;
    }

}