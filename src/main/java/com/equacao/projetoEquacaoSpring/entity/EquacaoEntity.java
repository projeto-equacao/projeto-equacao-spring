package com.equacao.projetoEquacaoSpring.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor  //novo
public class EquacaoEntity {

    @Id
    private String id;
    private double a;
    private double b;
    private double c;
    private Double delta;
    private Double x1;
    private Double x2;
    private String message;

    public EquacaoEntity(double a, double b, double c, Double delta, Double x1, Double x2, String message) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.delta = delta;
        this.x1 = x1;
        this.x2 = x2;
        this.message = message;
    }
}
